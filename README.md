# AppManutencao

Projeto destinado à avaliação de contratação para equipe de Sustentação - Softplan

Versão utilizada do Delphi: Tokyo 10.2. Pode ser utilizada outras versões, desde que o código seja compatível. 

## Como submeter uma correção 

- Faça o clone do projeto
- Crie um novo branch com o nome `Branch_[Nome]_[Sobrenome]`
- Resolva todos defeitos descritos abaixo
- Todos os `hints` e `warnings` do projeto devem ser resolvidos.
- Todos os `memory leaks` do projeto devem ser resolvidos. 
- Colocar uma correção por commit
- Em cada commit, deve ser adicionado um comentário com a seguinte estrutura: 
  * Defeito: [número do defeito]
  * Causa: explicar tecnicamente qual era a causa do problema; 
  * Solução: explicar tecnicamente qual foi a solução dada ao problema; 
- Faça push e crie um novo Merge Request com as correções 
  * Para criar um Merge Request, entrar em Merge Request no lado esquerdo do projeto no Gitlab. Em Source Branch selecionar o branch criado, e no Target a master.

## Defeitos

### Dataset Copy 

`Defeito 1: fazer as alterações do Dataset 1 serem replicadas automaticamente no Dataset 2`

### Dataset Loop

`Defeito 2: corrigir rotina que apaga números pares`

### Streams

`Defeito 3: erro de Out of Memory ao clicar no botão "Load 100"`

`Defeito 4: quando clica várias vezes no botão Load 100, ao tentar fechar o sistema ele não fecha`

### Exceptions/Performance

`Defeito 5: ao clicar no botão "Operação 1" está escondendo a exceção original. Alterar para mostrar a exceção correta no Memo1`

`Defeito 6: ao clicar em "Operação em lote" não deve parar o processamento caso dê algum erro na rotina. Caso apresente algum erro, sua classe (ClassName da exceção) e mensagem (Message da exceção) devem ser mostrados no fim do processamento, no Memo1`

`Defeito 7: substitua o "GetTickCount" por outra forma de "contar" o tempo de processamento`

`Defeito 8: melhorar performance do processamento quando utilizado o botão "Operação em lote"`

### Threads

`Defeito 9: crie um formulário com o nome da unit “Threads.pas” e nome do form “fThreads” e altere o form Main para abrir este novo form, como é feito nos outros botões. Neste form deve haver um botão que executará duas threads (aqui se entende thread, task, thread anônima, qualquer tipo de programação paralela). Estas threads irão realizar um laço de 0 até 100, onde a cada iteração do laço elas deverão aguardar (sleep) um tempo em milisegundos determinado pelo usuário (pode ser configurado em um TEdit). A cada iteração do laço, a thread deverá incrementar uma barra de progresso, com valor Max 200 (100 de cada thread). A mesma barra de progresso deve ser usada em ambas threads`

### Finalização

`Defeito 10: por fim, altere a versão do projeto para 0.0.0.1 (Debug e Release) e altere o projeto para sempre mostrar a versão atual no Caption do form principal. Exempo: "Foo - 0.0.0.1"`